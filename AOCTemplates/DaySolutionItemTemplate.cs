﻿using AdventOfCode2019.Business;
using System;
using System.Linq;

namespace AOCTemplates
{
    /// <summary>
    /// https://adventofcode.com/2019/day/1
    /// </summary>
    internal class DaySolutionItemTemplate : CodePuzzleSolution<object>
    {
        public DaySolutionItemTemplate(bool autoSolve = true) : base(0, null, autoSolve) { }

        public override void Solve()
        {
            if (!SolveNow || Data == null || !Data.Any())
                return;

            Console.WriteLine("# Day 0 #");
            Console.WriteLine($"Part A: {SolvePartA()}");
            Console.WriteLine($"Part B: {SolvePartB()}");
            Console.WriteLine("");
        }

        public int SolvePartA()
        {
            throw new NotImplementedException();
        }

        public int SolvePartB()
        {
            throw new NotImplementedException();
        }

    }
}
