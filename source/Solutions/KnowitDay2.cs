﻿using AdventOfCode2019.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AdventOfCode2019.Solutions
{
    public class KnowitDay2 : CodePuzzleSolution<string>
    {
        public KnowitDay2(bool autoSolve = true) : base(102, new LineParser<string>(), autoSolve) { }

        public override void Solve()
        {
            if (!SolveNow || Data == null || !Data.Any())
                return;

            Console.WriteLine("# Knowit Day 1 #");

            var counter = 0;

            foreach(var line in Data)
            {
                counter += CountSpaces(line);
            }

            Console.WriteLine("Result:" + counter);
        }

        private int CountSpaces(string line)
        {
            return line.Trim(' ').Count(c => c == ' ');
        }
    }
}
