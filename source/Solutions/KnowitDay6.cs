﻿using AdventOfCode2019.Business;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace AdventOfCode2019.Solutions
{
    public class KnowitDay6 : CodePuzzleSolution<string>
    {
        public KnowitDay6(bool autoSolve = true) : base(Constants.CUSTOM_DATA, null, autoSolve)
        {
            var bp = IO.GetBasePath();
            var path = bp + @"\Input\mush.png";
            ImageData = new Bitmap(path);
        }

        public override void Solve()
        {
            for (int j = ImageData.Height-1; j >= 0; j--)
            {
                for (int i = ImageData.Width -1; i >= 0; i--)
                {
                    var p1 = ImageData.GetPixel(i, j);

                    Color p2 = default;
                   
                    if (i > 0)
                        p2 = ImageData.GetPixel(i - 1, j);
                    else if (j > 0)
                        p2 = ImageData.GetPixel(ImageData.Width - 1, j - 1);

                    if(p2 != default)
                        ImageData.SetPixel(i, j, Color.FromArgb(p1.R ^ p2.R, p1.G ^ p2.G, p1.B ^ p2.B));
                }
            }

            ImageData.Save("result.png");
        }     
    }
}
