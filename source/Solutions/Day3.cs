﻿using AdventOfCode2019.Business;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2019.Solutions
{
    public class Day3 : CodePuzzleSolution<string>
    {
        public Day3(bool autoSolve = true) : base(3, new LineParser<string>(), autoSolve)
        {
        }

        public override void Solve()
        {
            if (!SolveNow || Data == null || !Data.Any())
                return;

            Console.WriteLine($"Part A: {SolvePartA()}");
            Console.WriteLine($"Part A: {SolvePartB()}");
        }

        protected static int origRow = Console.CursorTop;
        protected static int origCol = Console.CursorLeft;

        protected static void WriteAt(string s, int x, int y)
        {
            try
            {
                Console.SetCursorPosition(origCol + x, origRow + y);
                Console.Write(s);
            }
            catch (ArgumentOutOfRangeException e)
            {
                Console.Clear();
                Console.WriteLine(e.Message);
            }
        }

        private int SolvePartA()
        {
            var parser = new CommaParser<string>();

            var start = (0, 0);
            var vectorsFirstWire = CreateVectors(Data[0], parser);
            var vectorsSecondWire = CreateVectors(Data[1], parser);

            var smallestDistance = int.MaxValue;
            var (x, y) = (0, 0);

            Console.WriteLine("");
       
            foreach (var v in vectorsFirstWire)
            {
                foreach (var w in vectorsSecondWire)
                {
                    if (v.Item1 == w.Item1 && v.Item2 == w.Item2 && Dist(v, (0,0)) != 0 && Dist(v, (0, 0)) < smallestDistance)
                    {
                        smallestDistance = Dist(v, (0, 0));
                        x = v.Item1;
                        y = v.Item2;
                    }
                }
            }

            return Dist((x, y), (0, 0));

        }

        private static int Dist((int,int) v, (int, int) w)
        {
            return Math.Abs(v.Item1 - w.Item1) + Math.Abs(v.Item2 - w.Item2);
        }

        private List<(int, int)> CreateVectors(string input, CommaParser<string> parser)
        {
            List<(int, int)> result = new List<(int, int)>();

            var (x, y) = (0,0);
            foreach (var instr in parser.Parse(input))
            {
                var direction = instr[0..1][0];
                var length = int.Parse(instr[1..]);

                switch (direction)
                {
                    case 'U': 
                        for(var yS = y; yS > y - length; yS--) { result.Add((x, yS)); }
                        y -= length; 
                        break;
                    case 'R':
                        for (var xS = x; xS < x + length; xS++) { result.Add((xS, y)); }
                        x += length;
                        break;
                    case 'D':
                        for (var yS = y; yS < y + length; yS++) { result.Add((x, yS)); }
                        y += length; 
                        break;
                    case 'L':
                        for (var xS = x; xS > x - length; xS--) { result.Add((xS, y)); }
                        x -= length; 
                        break;
                }

               //result.Add((x, y));
            }

            return result;
        }

        private int SolvePartB()
        {
            return -1;
        }
    }
}
