﻿using AdventOfCode2019.Business;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace AdventOfCode2019.Solutions
{
    public class KnowitDay5 : CodePuzzleSolution<string>
    {
        public KnowitDay5(bool autoSolve = true) : base(105, new LineParser<string>(), autoSolve)
        {
        }

        private char[] arr;

        public override void Solve()
        {       
            if (!SolveNow || Data == null || !Data.Any())
                return;

            // Load the data from the input file to a char array
            arr = Data.First().ToCharArray();

            var sw = new Stopwatch();

            sw.Start();

            // Reverse step three mixing
            for (int i = 0; i < arr.Length / 2; i += 3)
            {
                Swap(i, arr.Length - i - 3);
                Swap(i + 1, arr.Length - i - 2);
                Swap(i + 2, arr.Length - i - 1);
            }

            // Reverse step two mixing
            for (int i = 0; i < arr.Length - 1; i += 2)
            {
                Swap(i, i + 1);             
            }

            // Reverse stop one mixing
            var halfs = new string(arr).SplitInHalf();
            var result = halfs[1] + halfs[0];

            sw.Stop();

            Console.WriteLine("Luke 5: " + result + " (" + sw.ElapsedMilliseconds + "ms)");
        }

        private void Swap(int i, int j)
        {
            var temp = arr[i];
            arr[i] = arr[j];
            arr[j] = temp;
        }
    }
}
