﻿using AdventOfCode2019.Business;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2019.Solutions
{
    public class Day2 : CodePuzzleSolution<int>
    {
        public Day2(bool autoSolve = true) : base(2, new CommaParser<int>(), autoSolve)
        {
        }

        public override void Solve()
        {
            if (!SolveNow || Data == null || !Data.Any())
                return;

            Console.WriteLine($"Part A: {SolvePartA(Data, 12, 2)}");
            Console.WriteLine($"Part A: {SolvePartB()}");
        }

        private int SolvePartA(List<int> data, int noun, int verb)
        {
            var values = (int[])data.ToArray().Clone();

            values[1] = noun;
            values[2] = verb;

            for (var i = 0; i < values.Length; i+=4)
            {
                switch (values[i])
                {
                    case 1: values[values[i + 3]] = values[values[i + 1]] + values[values[i + 2]]; break;
                    case 2: values[values[i + 3]] = values[values[i + 1]] * values[values[i + 2]]; break;
                    case 99: return values[0];       
                }
            }

            return -1;
        }

        private int SolvePartB()
        {
            const int TARGET = 19690720;

            for (int n = 0; n < 100; n++)
            {
                for (int v = 0; v < 100; v++)
                {
                    var t = SolvePartA(Data, n, v);

                    if (t == TARGET)
                        return 100 * n + v;
                }
            }

            return -1;
        }
    }
}
