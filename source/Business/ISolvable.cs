﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AdventOfCode2019.Business
{
    internal interface ISolvable
    {
       void Solve();
    }
}
