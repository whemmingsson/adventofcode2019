﻿using AdventOfCode2019.Business;
using AdventOfCode2019.Solutions;
using System;
using System.Collections.Generic;

namespace AdventOfCode2019
{
    class Program
    {
        private static readonly List<ISolvable> m_puzzles = new List<ISolvable>();

        static void Main()
        {
            Console.WriteLine("ADVENT OF CODE");
            Console.WriteLine("");

            m_puzzles.Add(new Day1(false));
            m_puzzles.Add(new Day2(false));
            m_puzzles.Add(new Day3(false));
            m_puzzles.Add(new KnowitDay2());
            m_puzzles.Add(new KnowitDay5(false));
            m_puzzles.Add(new KnowitDay6(false));

            m_puzzles.ForEach(p => p.Solve());
        }
    }
}
